package com.sangeng.task;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.sangeng.domain.LoginUser;
import com.sangeng.domain.MockRedis;
import com.sangeng.mapper.MockRedisMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;


@Component
@EnableScheduling //开启定时任务
public class MockRedisExpireTask {

//    public static final Logger LOGGER = LoggerFactory.getLogger(StaticScheduleTask.class);

    @Autowired
    private MockRedisMapper mockRedisMapper;

    @Scheduled(cron = "0/20 * * * * ?")
    public void deleteByExpireTime() {
        QueryWrapper<MockRedis> wrapper = new QueryWrapper<>();
        Integer i = mockRedisMapper.deleteByExpireTime();
        if (i > 0) {
            System.out.println(new Date());
        }
    }

    @Scheduled(cron = "0/1 * * * * ?")
    public void getMockRedis() {
        LambdaQueryWrapper<MockRedis> wrapper = new LambdaQueryWrapper<>();
        List<MockRedis> mockRedis = mockRedisMapper.selectList(wrapper);
        MockRedis redis = mockRedis.get(mockRedis.size() - 1);
        String v = redis.getV();
        if (!StringUtils.hasText(v)){
            LoginUser loginUser = JSONObject.parseObject(v, LoginUser.class);
            System.out.println(loginUser);
        }


    }

}
