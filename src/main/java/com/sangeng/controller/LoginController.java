package com.sangeng.controller;

import com.sangeng.domain.User;
import com.sangeng.domain.response.RespBean;
import com.sangeng.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;
    @PostMapping("/user/login")
    public RespBean login(@RequestBody User user){
        return loginService.login(user);
    }
    @PostMapping("/user/logout")
    public RespBean logout(@RequestBody User user){
        return loginService.logout();
    }
}
