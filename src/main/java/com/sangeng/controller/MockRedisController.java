package com.sangeng.controller;


import com.sangeng.domain.MockRedis;
import com.sangeng.domain.response.RespBean;
import com.sangeng.mapper.MockRedisMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author cd
 * @since 2024-06-21
 */
@RestController
@RequestMapping("/redis")
public class MockRedisController {

    @Autowired
    private MockRedisMapper mockRedisMapper;

    @GetMapping("/t1")
    @PreAuthorize("hasAuthority('t1:list')")
    public RespBean mock() {
        List<MockRedis> mockRedis = mockRedisMapper.selectList(null);
        return RespBean.success("message",mockRedis);
    }
    @GetMapping("/t2")
    @PreAuthorize("hasAuthority('t1:add')")
    public RespBean mock2() {
        List<MockRedis> mockRedis = mockRedisMapper.selectList(null);
        return RespBean.success("message",mockRedis);
    }

}
