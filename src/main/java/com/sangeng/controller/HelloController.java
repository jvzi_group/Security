package com.sangeng.controller;


import com.sangeng.domain.response.RespBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/t")
    public RespBean hello() {
        return RespBean.success("44");
    }
}
