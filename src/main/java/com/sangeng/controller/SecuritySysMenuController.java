package com.sangeng.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author cd
 * @since 2024-06-27
 */
@RestController
@RequestMapping("/security-sys-menu")
public class SecuritySysMenuController {

}
