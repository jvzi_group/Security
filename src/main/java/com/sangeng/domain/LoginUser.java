package com.sangeng.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class LoginUser implements UserDetails {
    private User user;

    //存储权限信息
    private List<String> permissions;

    public LoginUser(User user, List<String> permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    //存储SpringSecurity所需要的权限信息的集合
    @JSONField(serialize = false)
    private List<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (authorities != null) {
            return authorities;
        }
        //把permissions中字符串类型的权限信息转换成GrantedAuthority对象存入authorities 中
        authorities = permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /*注意：如果要测试，需要往用户表中写入用户数据，并且如果你想让用户的密码是明文存储，需要在密
    码前加{noop}。例如
    这样登陆的时候就可以用sg作为用户名，1234作为密码来登陆了。
            2.3.3.2 密码加密存储
    实际项目中我们不会把密码明文存储在数据库中。
    默认使用的PasswordEncoder要求数据库中的密码格式为：{id}password 。它会根据id去判断密码的
    加密方式。但是我们一般不会采用这种方式。所以就需要替换PasswordEncoder。
    我们一般使用SpringSecurity为我们提供的BCryptPasswordEncoder。
    我们只需要使用把BCryptPasswordEncoder对象注入Spring容器中，SpringSecurity就会使用该
    PasswordEncoder来进行密码校验。
    我们可以定义一个SpringSecurity的配置类，SpringSecurity要求这个配置类要继承
    WebSecurityConfigurerAdapter。*/
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
