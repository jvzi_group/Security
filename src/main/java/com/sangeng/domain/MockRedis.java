package com.sangeng.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author cd
 * @since 2024-06-21
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("mock_redis")
@ApiModel(value = "MockRedis对象", description = "")
public class MockRedis implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String k;

    private String v;

    private LocalDateTime expireTime;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;


}
