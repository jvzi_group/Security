package com.sangeng.mapper;

import com.sangeng.domain.SecuritySysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author cd
 * @since 2024-06-27
 */
@Mapper
public interface SecuritySysMenuMapper extends BaseMapper<SecuritySysMenu> {
    List<String> selectPermsByUserId(Long id);
}
