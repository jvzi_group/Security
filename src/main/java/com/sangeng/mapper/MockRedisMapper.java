package com.sangeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sangeng.domain.MockRedis;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cd
 * @since 2024-06-21
 */
@Mapper
public interface MockRedisMapper extends BaseMapper<MockRedis> {

    Integer deleteByExpireTime();

    List<MockRedis>  selectAll();

}
