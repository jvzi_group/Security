package com.sangeng.service.impl;

import com.sangeng.domain.SecuritySysMenu;
import com.sangeng.mapper.SecuritySysMenuMapper;
import com.sangeng.service.ISecuritySysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author cd
 * @since 2024-06-27
 */
@Service
public class SecuritySysMenuServiceImpl extends ServiceImpl<SecuritySysMenuMapper, SecuritySysMenu> implements ISecuritySysMenuService {

}
