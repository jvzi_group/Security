package com.sangeng.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sangeng.domain.MockRedis;
import com.sangeng.mapper.MockRedisMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sangeng.service.MockRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cd
 * @since 2024-06-21
 */
@Service
public class MockRedisServiceImpl extends ServiceImpl<MockRedisMapper, MockRedis> implements MockRedisService {
    @Autowired
    private MockRedisMapper redisMapper;

    @Override
    public String get(String k) {
        LambdaQueryWrapper<MockRedis> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(MockRedis::getK,k);
        MockRedis mockRedis = redisMapper.selectOne(wrapper);
        if (!Objects.isNull(mockRedis)){
            return mockRedis.getV();
        }
        return null;
    }

    @Override
    public int set(String k, String v) {
        MockRedis mockRedis = new MockRedis();
        mockRedis.setK(k);
        mockRedis.setV(v);
        return redisMapper.insert(mockRedis);

    }

    @Override
    public int set(String k, String v, Long expireDate) {
        MockRedis mockRedis = new MockRedis();
        mockRedis.setK(k);
        mockRedis.setV(v);
        LocalDateTime now = LocalDateTime.now();
        mockRedis.setExpireTime(now.plusSeconds(expireDate));
        return redisMapper.insert(mockRedis);
    }

    @Override
    public int delete(String k) {
        int i = redisMapper.delete(Wrappers.<MockRedis>query().lambda().eq(MockRedis::getK, k));
        return i;
    }
}
