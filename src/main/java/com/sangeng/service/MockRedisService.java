package com.sangeng.service;

import com.sangeng.domain.MockRedis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cd
 * @since 2024-06-21
 */
public interface MockRedisService extends IService<MockRedis> {

    String get(String k);

    int set(String k, String v);

    int set(String k, String v,Long expireDate);

    int delete(String k);

}
