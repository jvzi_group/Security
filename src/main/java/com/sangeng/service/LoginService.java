package com.sangeng.service;

import com.sangeng.domain.User;
import com.sangeng.domain.response.RespBean;

public interface LoginService {
    RespBean login(User user);

    RespBean logout();
}
