package com.sangeng.service;

import com.sangeng.domain.SecuritySysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author cd
 * @since 2024-06-27
 */
public interface ISecuritySysMenuService extends IService<SecuritySysMenu> {

}
