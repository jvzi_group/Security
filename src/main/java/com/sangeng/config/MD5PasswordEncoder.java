package com.sangeng.config;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.DigestUtils;


public class MD5PasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        //对密码进行 md5 加密
        String md5Password = DigestUtils.md5DigestAsHex(rawPassword.toString().getBytes());
        System.out.println("md5Password："+md5Password);
        return md5Password;
    }
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        // 通过md5校验
        System.out.println("rawPassword：" +rawPassword);
        System.out.println("encodedPassword："+encodedPassword);
        return encode(rawPassword).equals(encodedPassword);
    }

}
