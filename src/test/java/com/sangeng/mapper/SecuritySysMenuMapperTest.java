package com.sangeng.mapper;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SecuritySysMenuMapperTest {

    @Autowired
    private  MockRedisMapper mockRedisMapper;
    @Autowired
    private SecuritySysMenuMapper sysMenuMapper;

    @Test
    public void testMockRedisMapper(){
        List<String> strings = sysMenuMapper.selectPermsByUserId(1L);
        System.out.println(strings);
    }

}