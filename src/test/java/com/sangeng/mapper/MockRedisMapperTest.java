package com.sangeng.mapper;

import com.sangeng.domain.MockRedis;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MockRedisMapperTest {
    @Autowired
    private  MockRedisMapper mockRedisMapper;
    @Test
    public void testMockRedisMapper(){
        List<MockRedis> mockRedis = mockRedisMapper.selectList(null);
        System.out.println(mockRedis);
    }


}